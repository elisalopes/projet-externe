# Projet externe

## Présentation du projet :

Ce projet est un projet externe qui consiste à réaliser une application web mobile de livraison participative.
Les livraisons sont effectuées bénévolement par des particuliers pour des particuliers. L’objectif premier de
l’application est de privilégier la livraison aux personnes âgées et aux personnes à mobilité réduite. Elle
apporte également un soutien pour les commerçants qui n’ont eux plus besoin de passer par des services
payants pour effectuer ces livraisons. Quant aux livreurs, ils se verront gratifier sous forme de points
échangeables par la suite au près des organismes participatifs (mairie, magasins partenaires…) et
recevront des cadeaux (entrée au musée, réduction pour les transports en communs…).

<span style="color:orange">**Les besoins principaux sont :**</span>

➢ Une interface utilisateur destinée à trois types de personnes (livreurs, livrés, commerçants) qui
permet :
- Pour le livreur d’avoir accès à un système de géolocalisation pour situer les marchands
autour de lui, aux différentes commandes en cours, de pouvoir les accepter et les livrer. Une
fois une commande acceptée par le livreur, un chat se met en place entre le livré et lui.
- Pour le livré d’avoir accès à ce même système de géolocalisation, mais afin de pouvoir
sélectionner une boutique et d’y passer commande. Il peut également avoir accès au système
de réclamation si un problème survient.
- Pour le commerçant d’avoir accès à sa boutique ou il peut ajouter/modifier/supprimer des
articles. Il peut aussi accepter une ou plusieurs commandes.
- Pour n’importe quel utilisateur, un accès à une partie profil afin de modifier ses préférences.

➢ Une interface admin web sécurisé par un accès restreint qui permet :
- Création d’un utilisateur
- Modification des informations liés aux utilisateurs
- Suppression d’un utilisateur
- Visuel sur les différentes commandes et messages échangés.

## Credits
Projet effectué en équipe avec mes collègues de formation à Beweb
- Elisa LOPES (Géolocalisation, Chat)
