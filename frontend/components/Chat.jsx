import React, { useState, useCallback, useEffect, Text } from "react";
import { GiftedChat } from "react-native-gifted-chat";
import { io } from "socket.io-client";
import * as axios from "axios"; // For the requests

const ENDPOINT = "ws://192.168.1.45:3000"; //modify the path
const API = "http://192.168.1.45:5000";
// debug temp (remove it when the app is ready)
const idCustomer = "5fc61d048746e77b5c90e0f4";
const idDeliverer = "5fc61bc08746e77b5c90e0f0";

export default function Chat(self) {
  const [messages, setMessages] = useState([]);
  const userType = self.userType;
  const userOtherType = userType == "customer" ? "deliverer" : "customer";
  const idOrder = self.idOrder;
  const idUser = userType == "customer" ? idCustomer : idDeliverer; // CHANGE THE USERID AND USE THE CORRECT ONE
  const idOtherUser = userType == "customer" ? idDeliverer : idCustomer; // FIND A BETTER SOLUTION TO FIND THE OTHER USERID (COMPLICATED WITHOUT REAL DATA)
  const socket = io(ENDPOINT + "?idOrder=" + idOrder + "&idUser=" + idUser);
  const [isTyping, setTyping] = useState(false);
  let isWriting = false;

  // update the read state of the message and send an event to the other user
  function setMessageRead(userType, msgId) {
    axios
      .patch(API + "/api/message/" + userType + "/state/" + msgId, {
        state: "read"
      })
      .then();

    socket.emit(idOrder + "-" + idUser + "-reading", msgId);
  }

  useEffect(() => {
    axios
      .get(API + "/api/message/" + idOrder)
      .then(function(response) {
        let currentMessages = [];

        // TODO: improve this double for
        // delivery message
        for (let i = 0; i < response.data[0].messageDeliverer.length; i++) {
          let msg = response.data[0].messageDeliverer[i];
          msg.idUser = idDeliverer;
          currentMessages.push(msg);

          // set the message to readed
          if (userType == "customer" && msg.state == "unread") {
            setMessageRead("deliverer", msg._id);
          }
        }

        // customer message
        for (let i = 0; i < response.data[0].messageCustomer.length; i++) {
          let msg = response.data[0].messageCustomer[i];
          msg.idUser = idCustomer;
          currentMessages.push(msg);

          // set the message to readed
          if (userType == "deliverer" && msg.state == "unread") {
            setMessageRead("customer", msg._id);
          }
        }

        // order by date (desc)
        currentMessages.sort((a, b) => {
          return new Date(b.date) - new Date(a.date);
        });

        let formatedMessages = [];
        for (let i = 0; i < currentMessages.length; i++) {
          const msg = currentMessages[i];

          formatedMessages.push({
            _id: msg._id,
            text: msg.text,
            createdAt: msg.date,
            user: {
              _id: msg.idUser
            },
            sent: true,
            received: msg.state == "read"
          });
        }

        setMessages(formatedMessages);
      })
      .catch(function(error) {
        console.log(error);
      });

    // receive a new message
    socket.on(idOrder + "-" + idUser, function(msg) {
      setMessages(prevMessages => {
        if (!prevMessages) {
          prevMessages = [];
        }

        prevMessages = [
          {
            _id: msg._id,
            text: msg.text,
            createdAt: msg.date,
            user: {
              _id: msg.idUser
            }
          }
        ].concat(prevMessages);

        setMessageRead(userOtherType, msg._id);
        return prevMessages;
      });
    });

    // wait the other user readed his messages
    socket.on(idOrder + "-" + idOtherUser + "-reading", msgId => {
      setMessages(prevMessages => {
        // update the message we received
        // we need to use "{ ...el, received: true }" to force the refresh
        // el = element
        return prevMessages.map(el =>
          el._id === msgId ? { ...el, received: true } : el
        );
      });
    });

    // detect if the other user is writing
    socket.on(idOrder + "-" + idOtherUser + "-typing", value => {
      setTyping(value);
    });

    // disconnect if the user leaved
    return () => socket.disconnect();
  }, []);

  // new message to send
  const onSend = useCallback((messages = []) => {
    socket.emit(idOrder + "-" + idUser + "-sending", {
      _id: messages[0]._id,
      text: messages[0].text
    });
    messages[0].sent = true;
    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, messages)
    );
  }, []);

  // update typing event
  const onTextChanged = useCallback((text = "") => {
    // show the typing if 3 letters are written
    if (text.length >= 3 && !isWriting) {
      isWriting = true;
      socket.emit(idOrder + "-" + idUser + "-typing", true);
    }
    // hide the typing if 0 letter
    else if (text.length === 0 && isWriting) {
      isWriting = false;
      socket.emit(idOrder + "-" + idUser + "-typing", false);
    }
  }, []);

  return (
    <GiftedChat
      messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: idUser
      }}
      onInputTextChanged={text => onTextChanged(text)}
      isTyping={isTyping}
    />
  );
}
