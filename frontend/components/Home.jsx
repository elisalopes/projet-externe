import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { Actions } from "react-native-router-flux";

const Home = () => {
  const goToClientLocation = () => {
    Actions.ClientLocation();
  };

  const goToChatA = () => {
    Actions.Chat({ idOrder: "5fc620598746e77b5c90e0f6", userType: "customer" });
  };

  const goToChatB = () => {
    Actions.Chat({
      idOrder: "5fc620598746e77b5c90e0f6",
      userType: "deliverer"
    });
  };

  const goToHomepage = () => {
    Actions.Homepage();
  };

  const goToShopList = () => {
    Actions.ShopList();
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={goToClientLocation}>
        <Text style={styles.text}>Carte client</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={goToChatA}>
        <Text style={styles.text}>Customer</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={goToChatB}>
        <Text style={styles.text}>Deliverer</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={goToHomepage}>
        <Text style={styles.text}>Interface Livreur</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={goToShopList}>
        <Text style={styles.text}>ShopList</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
    marginTop: 100
  },
  button: {
    backgroundColor: "#79BCF6",
    borderRadius: 30,
    padding: 20,
    marginTop: 25,
    width: 150
  },
  text: {
    textAlign: "center"
  }
});

export default Home;
