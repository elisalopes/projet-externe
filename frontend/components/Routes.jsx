import React, { Component } from "react";
import { Router, Scene } from "react-native-router-flux";
import Home from "./Home";
import ClientLocation from "./Location";
import Chat from "./Chat";
import Homepage from "./Homepage";
import ShopList from "./ShopList";
import Order from "./Deliverer/Order";

const Routes = () => (
  <Router>
    <Scene key="root">
      <Scene key="Home" component={Home} title="Home" initial={true} />
      <Scene
        key="ClientLocation"
        component={ClientLocation}
        title="Client location"
      />
      <Scene key="Chat" component={Chat} title="Chat" />
      <Scene key="Homepage" component={Homepage} title="Homepage" />
      <Scene key="Order" component={Order} title="Order" />
      <Scene key="ShopList" component={ShopList} title="ShopList" />
    </Scene>
  </Router>
);

export default Routes;
