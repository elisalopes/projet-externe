import React, { useState, useEffect } from "react";
import * as Location from "expo-location"; // For the permissions
import MapView, { Marker } from "react-native-maps"; // For the map
import { Actions } from "react-native-router-flux"; // For the routers
import DropdownMenu from "react-native-dropdown-menu"; // For the dropdown
import { View } from "react-native";
import * as axios from "axios"; // For the requests

export default function ClientLocation() {
  const optionValue = [
    // refers to the dropdown
    [500, 1000, 2000],
    [null, "food", "other"]
  ];
  const optionText = [
    // refers to the dropdown
    ["500m", "1km", "2km"],
    ["Tout", "Alimentaire", "Autre"]
  ];
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [distance, setDistance] = useState(optionValue[0][0]);
  const [category, setCategory] = useState(optionValue[1][0]);

  useEffect(() => {
    // PERMISSIONS MANAGEMENT
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        Actions.Home();
      }

      let location = await Location.getCurrentPositionAsync({
        accuracy: Location.Accuracy.High
      });
      setLocation(location);
    })();
  }, []);

  const [markers, setMarkers] = useState([]);

  const updateDropdown = (row, selection) => {
    if (row == 0) {
      setDistance(optionValue[row][selection]);
    } else if (row == 1) {
      setCategory(optionValue[row][selection]);
    }
  };

  useEffect(() => {
    const addMarkers = () => {
      if (!location) {
        return;
      }
      axios
        .get(
          "http://10.0.2.2:5000/api/user/merchants/" +
            location.coords.latitude +
            "/" +
            location.coords.longitude +
            "/3"
        )
        .then(function(response) {
          //console.log(response.data);
          let points = response.data;
          if (category != null) {
            // Handle the filter
            let isFoodShop = category == "food";
            points = points.filter(shop => shop.foodShop == isFoodShop);
          }
          let current = [];
          for (let i = 0; i < points.length; i++) {
            let newDist = calculDistance(
              location.coords.latitude,
              location.coords.longitude,
              points[i].location.coordinates[0],
              points[i].location.coordinates[1]
            );

            if (newDist * 1000 < distance) {
              // Save in meters, convert distance in meters
              current.push(
                <Marker // Add shop markers
                  key={points[i]._id}
                  style={{ backgroundColor: "blue" }}
                  coordinate={{
                    latitude: points[i].location.coordinates[0],
                    longitude: points[i].location.coordinates[1]
                  }}
                  title={points[i].name}
                  description={points[i].category}
                  onCalloutPress={() => Actions.Order({ shop: points[i] })}
                  image={require("../assets/icone_boutique.png")}
                />
              );
            }
          }
          setMarkers(current); // Put markers in the right places
        })
        .catch(function(error) {
          console.log(error);
        });
    };
    addMarkers();
  }, [location, category, distance]);

  if (errorMsg || !location) {
    return null;
  }

  return (
    <View style={{ flex: 1 }}>
      <DropdownMenu // Filter display
        style={{ flex: 1 }}
        bgColor={"white"}
        tintColor={"#666666"}
        activityTintColor={"green"}
        handler={updateDropdown}
        data={optionText}
        useNativeDriver={true}
      >
        <MapView // Map moved in the dropdown, it prevented the display of the dropdown
          style={{
            flex: 1
          }}
          initialRegion={regionFrom(
            location.coords.latitude,
            location.coords.longitude,
            10000
          )}
        >
          <Marker
            style={{ backgroundColor: "red" }}
            coordinate={location.coords}
            title={"My Location"}
            description={"Hello, i'm here"}
          />
          {markers}
          <MapView.Circle // Scope management
            center={location.coords}
            radius={distance}
            strokeColor={"#1a66ff"}
            fillColor={"rgba(230,238,255,0.5)"}
          />
        </MapView>
      </DropdownMenu>
    </View>
  );
}

function regionFrom(lat, lon, distance) {
  // Calculate latitudeDelta and LongitudeDelta for the Mapview
  distance = distance / 2;
  const circumference = 40075;
  const oneDegreeOfLatitudeInMeters = 111.32 * 1000;
  const angularDistance = distance / circumference;

  const latitudeDelta = distance / oneDegreeOfLatitudeInMeters;
  const longitudeDelta = Math.abs(
    Math.atan2(
      Math.sin(angularDistance) * Math.cos(lat),
      Math.cos(angularDistance) - Math.sin(lat) * Math.sin(lat)
    )
  );

  return {
    latitude: lat,
    longitude: lon,
    latitudeDelta,
    longitudeDelta
  };
}

function calculDistance(lat1, lon1, lat2, lon2) {
  // Calculate distance between 2 coordinates
  var p = 0.017453292519943295; // Math.PI / 180
  var c = Math.cos;
  var a =
    0.5 -
    c((lat2 - lat1) * p) / 2 +
    (c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))) / 2;
  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}
