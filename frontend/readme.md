# Frontend

## Introduction 

Application réalisée en React Native avec la plateforme Expo. Elle permet de tester la partie géolocalisation et chat du projet.

## Installation 

* Installer NodeJS et executer la commande npm install pour installer les dépendances.

* Pour démarrer l'application faites : expo start